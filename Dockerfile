FROM alpine

ARG TARGETPLATFORM

RUN mkdir /server && chmod 755 /server
WORKDIR /server

COPY .build/code-server/${TARGETPLATFORM} code-server
COPY scripts/gl/tools-injector/gl_init_tools.sh init_tools.sh
COPY scripts/gl/tools-injector/gl_copy_tools.sh copy_tools.sh

ENTRYPOINT ["./copy_tools.sh"]
