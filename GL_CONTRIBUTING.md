# Contributing to GitLab Web IDE - VSCode Fork

## Goals

- Minimize changes made to this fork.
- Make it **very** clear **the location** and **reason** for any change made ontop of the base [vscode project](https://github.com/microsoft/vscode).

## Rules

- Any new files **must** be prefixed with `gl_` or `GL_`, with the exception of files that require a specific name (e.g. `README.md` or `.gitlab-ci.yml`).
- Changes made to pre-existing files must be surrounded in commented regions, starting with `gl_mod START` and ending with `gl_mod END`. Please include a reason for the change with links for more context. For example:

  ```patch
   function vscodeFoo(a, b) {
     const c = a.ref || 'DEFAULT';

     return {
       c,
  -    disabled: false,
  +    // gl_mod START
  +    // We need this `disabled` flag to be true, otherwise
  +    // users are able to accidentally break everything.
  +    // See issue: https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/12
  +    disabled: true
  +    // gl_mod END
     };
   }
  ```

- Every cohesive change must be documented in `GL_CHANGELOG.md`. Please include any links to issues, comments, or merge requests which will provide more context.

## Build

For more context, also see the [build instructions on VSCode's wiki](https://github.com/microsoft/vscode/wiki/How-to-Contribute#build)

### Prerequisites

- [NodeJS](https://nodejs.org/en/), version >=16.14.x and <17
- [Yarn](https://classic.yarnpkg.com/en/docs/install)

### Running the build process

For GitLab's Web IDE, we are only interested in the web build. We added a yarn script to
run all the parts needed to produce the main uncompressed output in `.build/vscode-web`.

```
yarn run gitlab:build-vscode-web
```

## CI Builds

### Dockerfile

- We use a docker image based on the official vscode pipelines: `vscodehub.azurecr.io/vscode-linux-build-agent:bionic-x64`, but with nodejs and yarn installed.
  - Source of our image is here: https://gitlab.com/gitlab-org/gitlab-build-images/-/blob/master/Dockerfile.gitlab-vscode-fork-node
  - Source of the parent image is here: https://github.com/microsoft/vscode-linux-build-agent/tree/main/bionic-x64

### NodeJS

- NOTE: See the following issue related to incompatibility of node versions: https://github.com/microsoft/azure-pipelines-tasks/issues/14824
