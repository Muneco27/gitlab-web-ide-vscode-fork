#!/usr/bin/env bash

# This scripts builds the tools-injector.

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o xtrace # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"
ROOT_DIR="$(cd "$SCRIPT_DIR/../.." && pwd)"

TOOLS_INJECTOR_ARTIFACTS_DIR="${ROOT_DIR}/.build/tools-injector"
mkdir -p "${TOOLS_INJECTOR_ARTIFACTS_DIR}"

# https://stackoverflow.com/a/3162500
IMAGE_NAME_WIHTOUT_PREFIX="${IMAGE_NAME##*/}"

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

# TODO: we don't build docker image for ARM in our CI yet. It works locally
# https://gitlab.com/gitlab-org/gitlab/-/issues/392693
docker build -t "${IMAGE_NAME}:$(cat FULL_VERSION)" .

# only push the image if we are on tag
# else, save docker image artifact to tar to be able to load it locally and test it.
if [ $CI_COMMIT_TAG ]; then
	docker push "${IMAGE_NAME}:$(cat FULL_VERSION)"
else
	DEST="${TOOLS_INJECTOR_ARTIFACTS_DIR}/${IMAGE_NAME_WIHTOUT_PREFIX}:$(cat FULL_VERSION).tar.gz"
	docker save "${IMAGE_NAME}:$(cat FULL_VERSION)" | gzip > "${DEST}"
fi
