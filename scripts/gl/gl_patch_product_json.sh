#!/usr/bin/env bash

# This script patches VS Code server product.json to avoid CSP issues
# https://gitlab.com/gitlab-org/gitlab/-/issues/424977

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o xtrace # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"
ROOT_DIR="$(cd "$SCRIPT_DIR/../.." && pwd)"

# For a tagged pipeline, use CI_COMMIT_SHA
if [ "${CI_COMMIT_TAG}" ]; then
	PATCH_PRODUCT_JSON_WITH_COMMIT_SHA="${CI_COMMIT_SHA}"
fi

# --always is required in cases where there are no tags available
# e.g. while rebasing with upstream
# https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/merge_requests/71#note_1741978758
# For a non-tagged pipeline, use the commit of the last tag if not over-ridden
if [ -z "${PATCH_PRODUCT_JSON_WITH_COMMIT_SHA}" ]; then
	PATCH_PRODUCT_JSON_WITH_COMMIT_SHA=$(cd "$ROOT_DIR" && git rev-list -n 1 $(git describe --abbrev=0 --tags --always))
fi

WEBVIEW_URL_KEY="webviewContentExternalBaseUrlTemplate"
WEBVIEW_URL_VALUE="https://{{uuid}}.cdn.web-ide.gitlab-static.net/web-ide-vscode/stable/${PATCH_PRODUCT_JSON_WITH_COMMIT_SHA}/out/vs/workbench/contrib/webview/browser/pre/"
PRODUCT_JSON_FILE="${ROOT_DIR}/product.json"

echo "Patching '${WEBVIEW_URL_KEY}' in '${PRODUCT_JSON_FILE}' to '${WEBVIEW_URL_VALUE}'"

# "sed" behaves differently on some distributions. Following approach is portable.
# https://stackoverflow.com/a/44864004
sed -i.bak "s#\"${WEBVIEW_URL_KEY}\": \".*\"#\"${WEBVIEW_URL_KEY}\": \"${WEBVIEW_URL_VALUE}\"#" "${PRODUCT_JSON_FILE}" && rm "${PRODUCT_JSON_FILE}.bak"
