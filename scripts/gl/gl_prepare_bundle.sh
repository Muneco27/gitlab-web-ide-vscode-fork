#!/usr/bin/env bash

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o xtrace # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

rm -rf .build/vscode-web
mkdir .build/vscode-web

# Step 1. Copy the compiled VSCode to the build directory
cp -r out-vscode-web-min .build/vscode-web/out

# Step 2. Copy the compiled built-in extensions to the build directory
cp -r .build/web/extensions .build/vscode-web/extensions

# Step 3. Copy the runtime web node_modules to the build directory
cp -r remote/web/node_modules .build/vscode-web
cp remote/web/package.json .build/vscode-web
