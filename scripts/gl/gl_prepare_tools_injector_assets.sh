#!/usr/bin/env bash

# This script prepares VS Code server assets to be packaged in editor
# injector docker images.

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o xtrace # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"
ROOT_DIR="$(cd "$SCRIPT_DIR/../.." && pwd)"

# TODO: we don't build docker image for ARM in our CI yet. It works locally
# https://gitlab.com/gitlab-org/gitlab/-/issues/392693
LINUX_ARM_PACKAGE_NAME="vscode-reh-web-linux-arm64-$(cat "${ROOT_DIR}/FULL_VERSION").tar.gz"
LINUX_AMD_PACKAGE_NAME="vscode-reh-web-linux-x64-$(cat "${ROOT_DIR}/FULL_VERSION").tar.gz"

mkdir -p "${ROOT_DIR}/.build/code-server/linux/arm64"
mkdir -p "${ROOT_DIR}/.build/code-server/linux/amd64"

cd "${ROOT_DIR}/.build/vscode-reh-web"
tar -zxf "${LINUX_ARM_PACKAGE_NAME}" --dir "${ROOT_DIR}/.build/code-server/linux/arm64"
tar -zxf "${LINUX_AMD_PACKAGE_NAME}" --dir "${ROOT_DIR}/.build/code-server/linux/amd64"

cd "${ROOT_DIR}"
