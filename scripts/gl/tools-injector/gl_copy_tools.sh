#!/bin/sh

# This script copies the tools to be injected into the workspace.
#
# It uses the following environment variables
# $GL_TOOLS_DIR - directory where the tools are copied. This directory contains files used by the "gl_init_tools.sh" script.

mkdir -p ${GL_TOOLS_DIR}
cp -R code-server ${GL_TOOLS_DIR}/
cp init_tools.sh ${GL_TOOLS_DIR}/
