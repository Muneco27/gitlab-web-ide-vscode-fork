/* eslint-disable header/header */
import { MessagePortLike } from 'vs/workbench/services/extensions/common/extensionHostProtocol';

/**
 * -------------------------------------------------------------------------------
 *   PLEASE NOTE: These types are referenced also in the Web IDE. Please see:
 *   https://gitlab.com/gitlab-org/gitlab-web-ide/-/blob/main/packages/vscode-bootstrap/src/utils/handleFetchFromMessagePort.ts
 * -------------------------------------------------------------------------------
 */
const TYPE_FETCH = 'gitlab-fetch' as const;
const TYPE_FETCH_RESPONSE = 'gitlab-fetch-response' as const;

interface FetchEventData {
	type: typeof TYPE_FETCH;
	requestId: string;
	url: string;
}

interface FetchResponseEventData {
	type: typeof TYPE_FETCH_RESPONSE;
	requestId: string;
	response: {
		body: ArrayBuffer;
		headers: [string, string][];
		status: number;
		statusText: string;
	};
}

const isFetchMessageResponse = (e: unknown): e is FetchResponseEventData => Boolean(e && typeof e === 'object' && 'type' in e && e.type === TYPE_FETCH_RESPONSE);

const createResponseFromEventData = ({ response }: FetchResponseEventData): Response => new Response(response.body, {
	headers: new Headers(response.headers),
	status: response.status,
	statusText: response.statusText,
});

let requestId = 0;

export const fetchFromMessagePort = (messagePort: MessagePortLike, url: string): Promise<Response> => {
	const currentRequestId = String(++requestId);

	return new Promise<Response>((resolve, reject) => {
		const onMessage = (e: MessageEvent) => {
			if (!isFetchMessageResponse(e.data) || e.data.requestId !== currentRequestId) {
				return;
			}

			messagePort.removeEventListener('message', onMessage);
			resolve(createResponseFromEventData(e.data));
		};

		const message: FetchEventData = {
			type: TYPE_FETCH,
			requestId: currentRequestId,
			url,
		};
		messagePort.addEventListener('message', onMessage);
		messagePort.postMessage(message);
	});
};
